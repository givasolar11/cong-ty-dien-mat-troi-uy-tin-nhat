<strong>GIVASOLAR</strong> là công ty điện năng lượng mặt trời chuyên cung cấp các sản phẩm và giải pháp với mục tiêu phấn đấu trở thành công ty hàng đầu trong lĩnh vực tại TPHCM cũng như trên Toàn Quốc, mang đến những giải pháp năng lượng tối ưu, hiệu quả, đáp ứng sự hài lòng tốt nhất cho quý khách hàng.

Trước tình hình giá điện tăng, nguồn năng lượng ngày càng cạn kiệt. Chúng tôi hiểu được điều đó và chọn khởi đầu với lĩnh vực đèn năng lượng mặt trời, chúng tôi luôn tìm tòi những sản phẩm hữu ích mà khách hàng cần, phỏng đoán hướng đi của nó để giúp tăng trưởng kinh doanh một cách nhanh chóng cũng như xây dựng và củng cố thương hiệu cho doanh nghiệp mình. Nếu như bạn là một khách hàng thì chúng tôi cho bạn thấy tìm năng dòng sản phẩm mà chúng tôi mang đến có thể giúp bạn đột phát như thế nào.

Hơn thế chúng tôi luôn tìm kiếm những sản phẩm mới cũng như giải pháp mới hiệu quả khác nhằm mục đích cuối cùng là mang đến những gì khách hàng cần – giải pháp tốt cho khách hàng.

<strong><u>Các sản phẩm và dịch vụ cung cấp:</u></strong>

+ Đèn Năng Lượng Mặt Trời: <a href="https://givasolar.com/danh-muc/den-nang-luong-mat-troi/">https://givasolar.com/danh-muc/den-nang-luong-mat-troi/</a> hiện đại thông minh dần thay thế cho dòng đèn điện hiện nay, là giải pháp số 1 cho chiếu sáng không gian ngoài trời hiện nay. Chúng tôi cung cấp dòng sản phẩm về đèn: đèn đường, đèn pha, đèn cổng, đèn treo tường, đèn âm sàn, đèn trụ sân vườn, đèn cầm tay, đèn báo hiệu giao thông, các loại dây đèn trang trí,... Tất cả sản phẩm này đều sử dụng 100% năng lượng mặt trời và không cần đến dây điện để lắp đặt.

+ Pin năng lượng mặt trời: <a href="https://givasolar.com/danh-muc/tam-pin-nang-luong-mat-troi-cao-cap/">https://givasolar.com/danh-muc/tam-pin-nang-luong-mat-troi-cao-cap</a> chuyên dùng trong lắp đặt, GivaSolar cung cấp tấm pin hãng GivaSolar đặt gia công sản xuất, pin chính hãng, chất lượng và hiệu suất cao, được khách hàng đánh giá cao. Chúng tôi cung cấp 2 dòng pin POLY và MONO với nhiều mức công suất từ mini: 40W, 50W, 60W đến trung bình: 100W, 150W, 200W đến những loại công suất cao chuyên dùng cho lắp đặt: 330W, 380W, 400W.

+ Lắp điện mặt trời hòa lưới <a href="https://givasolar.com/danh-muc/he-thong-nang-luong-mat-troi/">https://givasolar.com/danh-muc/he-thong-nang-luong-mat-troi</a> -  giải pháp đầu tư hiệu quả hiện nay trên các mái nhà. Là sử dụng điện thu được từ năng lượng mặt trời sau đó chúng được kết nối vào lưới điện quốc gia. Lượng điện được tạo ra luôn được dùng trước cho các hoạt động sinh hoạt… cho đến khi hết điện này thì hệ thống sẽ tự lấy điện lưới quốc gia để sử dụng. Trong trường hợp hệ thống sản xuất điện từ mặt trời dư thừa so với mức tiêu thụ cần thiết, thì năng lượng dư sẽ hòa vào lưới điện quốc gia và bạn sẽ được nhà nước chi trả cho lượng dư này.

<span style="text-decoration: underline;"><strong>Cùng với giải pháp này thì đi kèm cung cấp các link kiện chuyên dùng cho lắp đặt như:</strong></span>
<ul>
 	<li>Solar inverter: <a href="https://givasolar.com/danh-muc/bo-kich-dien-inverter/">https://givasolar.com/danh-muc/bo-kich-dien-inverter/</a></li>
 	<li>Bình acquy dự trữ năng lượng: <a href="https://givasolar.com/danh-muc/ac-quy-nang-luong-mat-troi/">https://givasolar.com/danh-muc/ac-quy-nang-luong-mat-troi/</a></li>
 	<li>Bộ điều khiển sạc NLMT: <a href="https://givasolar.com/danh-muc/bo-dieu-khien-sac-nang-luong-mat-troi/">https://givasolar.com/danh-muc/bo-dieu-khien-sac-nang-luong-mat-troi/</a></li>
 	<li>Phụ kiện điện mặt trời: <a href="https://givasolar.com/danh-muc/phu-kien-lap-dat-dien-mat-troi/">https://givasolar.com/danh-muc/phu-kien-lap-dat-dien-mat-troi/</a></li>
 	<li>Biến tần hệ thống bơm nước: <a href="https://givasolar.com/danh-muc/bien-tan-bom-nuoc/">https://givasolar.com/danh-muc/bien-tan-bom-nuoc/</a></li>
 	<li>Tủ điện Solar: <a href="https://givasolar.com/danh-muc/phu-kien-lap-dat-dien-mat-troi/tu-dien/">https://givasolar.com/danh-muc/phu-kien-lap-dat-dien-mat-troi/tu-dien/</a></li>
 	<li>Dây cáp điện Solar: <a href="https://givasolar.com/danh-muc/phu-kien-lap-dat-dien-mat-troi/day-cap-dien/">https://givasolar.com/danh-muc/phu-kien-lap-dat-dien-mat-troi/day-cap-dien/</a></li>
</ul>
Chúng tôi luôn không ngừng hoàn thiện mình về chất lượng sản phẩm cũng như chất lượng dịch vụ để thực hiện sứ mệnh mình một cách tốt nhất. Chúng tôi tin tưởng rằng sẽ giúp rất nhiều sản phẩm/dịch vụ có ích của Givasolar sẽ được đến tay nhiều người tiêu dùng hơn, nhanh hơn.

Từ khi thành lập đến nay, chúng tôi đã bán ra hàng nghìn sản phẩm, lắp đặt hàng chục dự án lớn nhỏ. Những gì chúng tôi trao đi cũng là những động lực và kinh nghiệm mà chúng tôi được nhận lại.

Facebook: <a href="https://www.facebook.com/DienNangLuongMatTroiGivaSolar/">https://www.facebook.com/DienNangLuongMatTroiGivaSolar/</a>

Website: <a href="https://givasolar.com/">https://givasolar.com/</a>

Địa chỉ: 569-571-573 Lê Trọng Tấn, Bình Hưng Hoà, Bình Tân, Hồ Chí Minh 700000

Điện thoại: <strong>1900 2680 – 0909 636 011</strong>

Mail: givasolar@gmail.com